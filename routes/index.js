var express = require('express');
var router = express.Router();
var User = require('../models/user');

var score = [
	100, 100, 100 ,100, 100, 100, 200, 200, 200 ,200, 200, 200, 300, 300, 300 ,300, 300, 300, 800, 800
];
var answer = [
"!","answer2","answer3","answer4","answer5","answer6","answer1","answer2","answer3","answer4","answer5","answer6","answer1","answer2","answer3","answer4","answer5","answer6","a","b"

];


function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		//req.flash('error_msg','You are not logged in');
		res.redirect('/users/login');
	}
}

// Get Homepage
router.get('/', ensureAuthenticated, function(req, res){
	res.render('rules');
});

//level page
router.get('/level', ensureAuthenticated, function(req, res){
	res.render('level',{user:req.user,message:''});
});

//redirect to level
router.get('/level1',function(req,res){
	res.redirect('level')

});
router.get('/level2',function(req,res){
	res.redirect('level')

});
router.get('/level3',function(req,res){
	res.redirect('level')

});

//rules page
router.get('/rules',ensureAuthenticated ,function(req,res){
	res.render('rules');

});


//Level 1 start

router.post('/level1', function(req, res){
	if(req.user.Totalscore >= 400)
	{
		res.render("level",{user:req.user, message: "The maximum score for Level 1 is 400 !!"});
	}
	else{

	var u_answer = req.body.answer;
	var question_no = req.body.question;

	var dones=req.user.done;
	console.log(dones);
	var flag=0;
	for(var i=0;i<dones.length;i++)
	{
		if(dones[i]==question_no){
			console.log('alreadydone');
			flag=1;
			res.render("level",{user:req.user,message: "Already Submitted"});
			break;
		}
	}
	if(flag==0){
	if(answer[question_no] == u_answer){
		console.log("going");
		console.log("flag "+flag);

		var new_score=req.user.Totalscore + score[question_no];

		User.update({_id: req.user._id},
		{
			$push: {
				done:question_no
			},
			$set: {
				Totalscore: new_score
			}
		},function(err, user){
				if(err) throw err;
				console.log("updated "+user);
				User.getUserById(req.user._id,function(err,user){
					console.log("check_score "+user.Totalscore);
					res.render('level',{user:user,message:'Successfully submitted'});
				});
		});
		
	}
	else {
		var url='Q'+question_no;
		res.render(url,{message:"Wrong answer"});
	}
}

  }
	
});

router.get('/Q0',ensureAuthenticated ,function(req,res){
	res.render('Q0',{user:req.user,message:''});

});

router.get('/Q1',ensureAuthenticated ,function(req,res){
	res.render('Q1',{user:req.user,message:''});

});
router.get('/Q2',ensureAuthenticated ,function(req,res){
	res.render('Q2',{user:req.user,message:''});

});
router.get('/Q3',ensureAuthenticated ,function(req,res){
	res.render('Q3',{user:req.user,message:''});

});
router.get('/Q4',ensureAuthenticated ,function(req,res){
	res.render('Q4',{user:req.user,message:''});

});
router.get('/Q5',ensureAuthenticated ,function(req,res){
	res.render('Q5',{user:req.user,message:''});

});

//level2 start

function level2Check(req, res, next){
	if(req.isAuthenticated()){
		console.log(req.user.Totalscore);
		if(req.user.Totalscore<400){
			res.render('level',{user:req.user,message:'Your score is not sufficient for level 2(<400)'})
		}
		else{
			return next();
		}

	}
	 else {
		//req.flash('error_msg','You are not logged in');
		res.redirect('/users/login');
	}
}


router.post('/level2', function(req, res){
	if(req.user.Totalscore >= 1200)
	{
		res.render("level",{user:req.user, message: "The maximum score for Level2 is 1200 !!"});
	}
	else{

	var u_answer = req.body.answer;
	var question_no = req.body.question;

	var dones=req.user.done;
	console.log(dones);
	var flag=0;
	for(var i=0;i<dones.length;i++)
	{
		if(dones[i]==question_no){
			console.log('alreadydone');
			flag=1;
			res.render("level",{user:req.user,message: "Already Submitted"});
			break;
		}
	}
	if(flag==0){
	if(answer[question_no] == u_answer){
		console.log("going");
		console.log("flag "+flag);

		var new_score=req.user.Totalscore + score[question_no];

		User.update({_id: req.user._id},
		{
			$push: {
				done:question_no
			},
			$set: {
				Totalscore: new_score
			}
		},function(err, user){
				if(err) throw err;
				console.log("updated "+user);
				User.getUserById(req.user._id,function(err,user){
					console.log("check_score "+user.Totalscore);
					res.render('level',{user:user,message:'Successfully submitted'});
				});				

		});
		
	}
	else {
		var url='Q'+question_no;
		res.render(url,{message:"Wrong answer"});
	}
}

  }
	
});


router.get('/Q6',level2Check ,function(req,res){
	res.render('Q6',{user:req.user,message:''});

});

router.get('/Q7',level2Check ,function(req,res){
	res.render('Q7',{user:req.user,message:''});

});
router.get('/Q8',level2Check ,function(req,res){
	res.render('Q8',{user:req.user,message:''});

});
router.get('/Q9',level2Check ,function(req,res){
	res.render('Q9',{user:req.user,message:''});

});
router.get('/Q10',level2Check ,function(req,res){
	res.render('Q10',{user:req.user,message:''});

});
router.get('/Q11',level2Check ,function(req,res){
	res.render('Q11',{user:req.user,message:''});

});

//level3 start

function level3Check(req, res, next){
	if(req.isAuthenticated()){
		console.log(req.user.Totalscore);
		if(req.user.Totalscore<1200){
			res.render('level',{user:req.user,message:'Your score is not sufficient for level 3(<1200)'})
		}
		else{
			return next();
		}

	}
	 else {
		//req.flash('error_msg','You are not logged in');
		res.redirect('/users/login');
	}
}


router.post('/level3', function(req, res){
	if(req.user.Totalscore >= 2400)
	{
		res.render("level",{user:req.user, message: "The maximum score for Level3 is 2400 !!"});
	}
	else{

		var u_answer = req.body.answer;
		var question_no = req.body.question;

		var dones=req.user.done;
		console.log(dones);
		var flag=0;
		for(var i=0;i<dones.length;i++)
		{
			if(dones[i]==question_no){
				console.log('alreadydone');
				flag=1;
				res.render("level",{user:req.user,message: "Already Submitted"});
				break;
			}
		}
		if(flag==0){

		if(answer[question_no] == u_answer){
			console.log("going");
			console.log("flag "+flag);

			var new_score=req.user.Totalscore + score[question_no];

			User.update({_id: req.user._id},
			{
				$push: {
					done:question_no
				},
				$set: {
					Totalscore: new_score
				}
			},function(err, user){
					if(err) throw err;
					console.log("updated "+user);
					User.getUserById(req.user._id,function(err,user){
						console.log("check_score "+user.Totalscore);
						res.render('level',{user:user,message:'Successfully submitted'});
					});

			});
			
		}
		else {
			var url='Q'+question_no;
			res.render(url,{message:"Wrong answer"});
		}

	}

  }
	
});


router.get('/Q12',level3Check ,function(req,res){
	res.render('Q12',{user:req.user,message:''});

});

router.get('/Q13',level3Check ,function(req,res){
	res.render('Q13',{user:req.user,message:''});

});
router.get('/Q14',level3Check ,function(req,res){
	res.render('Q14',{user:req.user,message:''});

});
router.get('/Q15',level3Check ,function(req,res){
	res.render('Q15',{user:req.user,message:''});

});
router.get('/Q16',level3Check ,function(req,res){
	res.render('Q16',{user:req.user,message:''});

});
router.get('/Q17',level3Check ,function(req,res){
	res.render('Q17',{user:req.user,message:''});

});

//level4

function level4Check(req, res, next){
	if(req.isAuthenticated()){
		console.log(req.user.Totalscore);
		if(req.user.Totalscore<2400){
			res.render('level',{user:req.user,message:'Your score is not sufficient for level 4(<2400)'})
		}
		else{
			return next();
		}

	}
	 else {
		//req.flash('error_msg','You are not logged in');
		res.redirect('/users/login');
	}
}


router.post('/level4', function(req, res){
	if(req.user.Totalscore >= 4000)
	{
		res.render("level",{user:req.user, message: "The maximum score for Level4 is 4000 !!"});
	}
	else{

		var u_answer = req.body.answer;
		var question_no = req.body.question;

		var dones=req.user.done;
		console.log(dones);
		var flag=0;
		for(var i=0;i<dones.length;i++)
		{
			if(dones[i]==question_no){
				console.log('alreadydone');
				flag=1;
				res.render("level",{user:req.user,message: "Already Submitted"});
				break;
			}
		}
		if(flag==0){
		if(answer[question_no] == u_answer){
			console.log("going");
			console.log("flag "+flag);

			var new_score=req.user.Totalscore + score[question_no];

			User.update({_id: req.user._id},
			{
				$push: {
					done:question_no
				},
				$set: {
					Totalscore: new_score
				}
			},function(err, user){
					if(err) throw err;
					console.log("updated "+user);
					User.getUserById(req.user._id,function(err,user){
						console.log("check_score "+user.Totalscore);
						if(user.Totalscore >= 4000){res.redirect('/success');}
						else{
						res.render('level',{user:user,message:'Successfully submitted'});
						}
					});

			});

		}
		else {
			var url='Q'+question_no;
			res.render(url,{message:"Wrong answer"});
		}
	}

  }
	
});

router.get('/Q18',level4Check ,function(req,res){
	res.render('Q18',{user:req.user,message:''});

});
router.get('/Q19',level4Check ,function(req,res){
	res.render('Q19',{user:req.user,message:''});

});

//winner start

function winnerCheck(req, res, next){
	if(req.isAuthenticated()){
		console.log(req.user.Totalscore);
		if(req.user.Totalscore<4000){
			res.render('level',{user:req.user,message:'LOL! WELL TRIED! '});
		}
		else{
			return next();
		}

	}
	 else {
		//req.flash('error_msg','You are not logged in');
		res.redirect('/users/login');
	}
}

router.get('/success',winnerCheck ,function(req,res){
	
	res.render('success',{user:req.user,message:''});

});


module.exports = router;